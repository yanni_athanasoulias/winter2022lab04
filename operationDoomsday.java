public class operationDoomsday {
	//This is the title for one of my favourite music albums
	private String album;
	private String artist;
	private String bestSong;
	public operationDoomsday(String album,String artist,String bestSong) {
		this.album = album;
		this.artist = artist;
		this.bestSong = bestSong;
	}
	
	public void setAlbum(String album) {
		this.album = album;
	}
	public String getAlbum() {
		return this.album;
	}
	public void setArtist(String artist) {
		this.artist = artist;
	}
	public String getArtist() {
		return this.artist;
	}
	public void setBestSong(String bestSong) {
		this.bestSong = bestSong;
	}
	public String getBestSong() {
		return this.bestSong;
	}
}